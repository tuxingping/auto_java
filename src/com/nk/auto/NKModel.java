package com.nk.auto;

public class NKModel {

	private String componentName;
	private String className;
	private String ridName;
	private boolean titleBar;
	private boolean titleBarSubView;

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getRidName() {
		return ridName;
	}

	public void setRidName(String ridName) {
		this.ridName = ridName;
	}

	public boolean isTitleBar() {
		return titleBar;
	}

	public void setTitleBar(boolean titleBar) {
		this.titleBar = titleBar;
	}

	public boolean isTitleBarSubView() {
		return titleBarSubView;
	}

	public void setTitleBarSubView(boolean titleBarSubView) {
		this.titleBarSubView = titleBarSubView;
	}

	@Override
	public String toString() {
		return NKModel.class.getSimpleName() + "[ Class = " + className + ","
				+ " ComponentName = " + componentName + "," + " Rid = "
				+ ridName + "," + " titleBar = " + titleBar + ","
				+ "titleBarSubView = " + titleBarSubView + "]";
	}

}
