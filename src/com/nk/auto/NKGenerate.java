package com.nk.auto;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class NKGenerate {

	private static final String TAG = NKGenerate.class.getSimpleName();

	// Dirs
	private String project_dir;
	private String scr_dir;
	private String res_dir;
	private String layouts_dir;

	// Names
	private String root_name;
	private String s_name;
	private String package_name;
	private String author;

	// Name space
	private String name_space_prefix;
	private String name_space_uri;

	// Theme
	private String theme_name;
	private String theme_value;

	// Navi
	private String navi_layout_xml;
	private String navi_bar_name;

	// Imports
	private ArrayList<String> import_list = new ArrayList<String>();

	// XML layout name prefix
	private ArrayList<String> xml_prefix_list = new ArrayList<String>();
	private String xml_activity_prefix;
	private String xml_item_prefix;
	private String xml_fragment_prefix;
	private String xml_fragementactivity_prefix;

	// Layout root
	private ArrayList<String> layout_root_list = new ArrayList<String>();

	// ActivityGroup
	private ArrayList<String> activity_group_list = new ArrayList<String>();

	// Generate auto java
	public void generate(HashMap<String, String> params,
			ArrayList<String> import_list,
			HashMap<String, String> xml_prefix_map,
			ArrayList<String> layout_root_list,
			ArrayList<String> activity_group_list) {

		// Dir
		project_dir = params.get(NKCommon.PROJECT_DIR);
		scr_dir = project_dir + File.separator + "src" + File.separator;
		res_dir = project_dir + File.separator + "res" + File.separator;
		layouts_dir = res_dir + "layout";

		// Names
		root_name = params.get(NKCommon.ROOT_NAME);
		s_name = root_name + ".java";
		package_name = params.get(NKCommon.PACKAGE_NAME);
		author = params.get(NKCommon.AUTHOR);
		navi_layout_xml = params.get(NKCommon.NAVI_LAYOUT_XML);

		// Name space
		name_space_prefix = params.get(NKCommon.NAME_SPACE_PREFIX);
		name_space_uri = NKCommon.ANDROID_NAMESPACE + package_name;

		// Theme
		theme_name = params.get(NKCommon.THEME_NAME);
		theme_value = params.get(NKCommon.THEME_VALUE);

		// Navi
		navi_layout_xml = params.get(NKCommon.NAVI_LAYOUT_XML);
		navi_bar_name = params.get(NKCommon.NAVI_BAR_NAME);

		// Imports
		this.import_list = import_list;

		// XML layout name prefix
		xml_prefix_list.addAll(xml_prefix_map.values());
		xml_activity_prefix = xml_prefix_map.get(NKCommon.XML_ACTIVITY_PREFIX);
		xml_item_prefix = xml_prefix_map.get(NKCommon.XML_ITEM_PREFIX);
		xml_fragment_prefix = xml_prefix_map.get(NKCommon.XML_FRAGMENT_PREFIX);
		xml_fragementactivity_prefix = xml_prefix_map
				.get(NKCommon.XML_FRAGMENTACTIVITY_PREFIX);

		// Layout roots
		this.layout_root_list = layout_root_list;

		// ActivityGroup
		this.activity_group_list = activity_group_list;

		StringBuffer content = new StringBuffer();
		// Add file comment.
		content.append(getHeaderJavadoc());

		// Add file package and import.
		content.append(getPackageAndImport(this.import_list));

		// Add public class
		content.append(getPublicInterface());

		// Get all generate xml files
		HashSet<String> xmlSet = filterAllXmlLayout(layouts_dir);

		// Generate inner class
		content.append(xml2Class(xmlSet));

		// Finish generate
		content.append("}\r\n ");
		// Generate CLASS_NAME_PREFIX.java

		writeFile(content, package_name, s_name);
	}

	/**
	 * Generate CLASS_NAME_PREFIX.java header comment
	 * 
	 * @return
	 */
	private String getHeaderJavadoc() {
		StringBuffer comment = new StringBuffer();
		comment.append("/**\r\n");
		comment.append(" * @author " + author + "\r\n");
		comment.append(" * @date " + new Date() + "\r\n");
		comment.append(" */\r\n\r\n");
		return comment.toString();
	}

	/**
	 * Generate CLASS_NAME_PREFIX.java package & import
	 * 
	 * @return
	 */
	private String getPackageAndImport(ArrayList<String> import_list) {

		if (import_list == null || import_list.isEmpty()) {
			NKLog.e(TAG, "getPackageAndImport :: import_list is null or empty");
			System.exit(1);
		}

		StringBuffer buffer = new StringBuffer();
		buffer.append("package " + package_name + ";\r\n\r\n");
		for (String tmp : import_list) {
			buffer.append("import " + tmp + ";\r\n");
		}
		buffer.append("\r\n");

		return buffer.toString();
	}

	/**
	 * Generate CLASS_NAME.java public interface
	 * 
	 * @return
	 */
	private String getPublicInterface() {
		return "public interface " + root_name + " {\r\n" + "\r\n";
	}

	/**
	 * Write auto generate content to CLASS_NAME.java
	 * 
	 * @param content
	 * @param packageName
	 * @param fileName
	 */
	private void writeFile(StringBuffer content, String packageName,
			String fileName) {

		if (packageName == null) {
			NKLog.e(TAG,
					"writeFile :: Package name is null, somethings wrong!!!");
			return;
		}

		if (fileName == null) {
			NKLog.e(TAG, "writeFile :: File name is null, something wrong!!!");
			return;
		}

		StringBuffer filePath = new StringBuffer(scr_dir);
		packageName = packageName.replace(".", "/");
		filePath = filePath.append(packageName);
		filePath = filePath.append("/" + fileName);
		File oldFile = new File(filePath.toString());
		// Delete old file.
		if (oldFile.exists()) {
			boolean result = oldFile.delete();
			if (result) {
				NKLog.o(TAG, "writeFile :: Delete old " + fileName
						+ " success!!!");
			} else {
				NKLog.e(TAG, "writeFile :: Delete old " + fileName
						+ "failed!!!");
				return;
			}
		}
		// Create new file.
		File newFile = new File(filePath.toString());
		NKLog.o(TAG,
				"writeFile :: " + s_name + " path is " + filePath.toString());
		if (!newFile.exists()) {
			try {
				newFile.createNewFile();
				OutputStream os = new BufferedOutputStream(
						new FileOutputStream(newFile));
				os.write(content.toString().getBytes());
				os.flush();
				os.close();
				NKLog.o(TAG, "writeFile :: Auto generate " + s_name + " ok!!!");
			} catch (IOException e) {
				NKLog.t(e);
				NKLog.e(TAG, "writeFile :: Auto generate " + s_name
						+ " failed!!!");
			}
		}
	}

	/**
	 * Filter all layout file name in res/layout
	 * 
	 * @param layouts_dir
	 * @return
	 */
	private HashSet<String> filterAllXmlLayout(String layouts_dir) {

		if (layouts_dir == null || layouts_dir.isEmpty()) {
			NKLog.e(TAG, "filterAllXmlLayout :: layouts_dir is null or empty");
			System.exit(1);
		}

		File file = new File(layouts_dir);
		String[] xmlArray = file.list();
		HashSet<String> xmlSet = new HashSet<String>();
		// Filter xml files
		for (String xml : xmlArray) {
			for (String prefix : xml_prefix_list) {
				if (xml.startsWith(prefix)) {
					xmlSet.add(xml);
				}
			}
		}

		return xmlSet;
	}

	/**
	 * Translate xml to class
	 * 
	 * @param xmlSet
	 * @return
	 */
	private String xml2Class(HashSet<String> xmlSet) {

		if (xmlSet == null || xmlSet.isEmpty()) {
			NKLog.e(TAG, "xml2Class :: xmlSet is null or empty");
			System.exit(1);
		}
		NKLog.o(TAG, "xml2Class :: xmlSet =" + xmlSet);

		StringBuffer buffer = new StringBuffer();
		for (String xml : xmlSet) {

			// xmlName to className
			String className = xmlName2ClassName(xml);

			// XML => Activity
			if (xml.startsWith(xml_activity_prefix)
					|| xml.startsWith(xml_fragementactivity_prefix)) {

				// xml to compoents
				List<NKModel> models = xml2Components(xml);

				// Class start
				boolean isGroupActivity = false;
				for (NKModel model : models) {
					if (activity_group_list.contains(model.getClassName())) {
						isGroupActivity = true;
						break;
					}
				}
				// FragmentActivity
				if (xml.startsWith(xml_fragementactivity_prefix)) {
					buffer.append("\n\n\t" + "abstract class " + className
							+ " extends FragmentActivity {  \n ");
				}
				// ActivityGroup
				else if (isGroupActivity) {
					buffer.append("\n\n\t" + "abstract class " + className
							+ " extends ActivityGroup {  \n ");
				}
				// Activity
				else {
					buffer.append("\n\n\t" + "abstract class " + className
							+ " extends Activity {  \n ");
				}

				// Generate pulbic params component.
				for (NKModel model : models) {
					buffer.append("\n\t\tpublic " + model.getClassName() + " "
							+ model.getComponentName() + ";");
				}

				// Generate onCreate().
				buffer.append("\n\n\t\t@Override");
				buffer.append("\n\t\tprotected void onCreate(Bundle savedInstanceState) {");
				buffer.append("\n\n\t\t\tsuper.onCreate(savedInstanceState);");
				buffer.append("\n\n\t\t\tgetWindow().setBackgroundDrawableResource(R.drawable.bg);\n");

				// Init param with findViewById().
				NKModel titleBarModel = null;
				for (NKModel nkModel : models) {
					if (nkModel.isTitleBar()) {
						titleBarModel = nkModel;
					}
				}

				// Have navi bar.
				if (titleBarModel != null) {
					buffer.append("\n\t\t\tLayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);");
					buffer.append("\n\t\t\tView " + NKCommon.CONTENT_VIEW
							+ " = inflater.inflate(R.layout."
							+ getLayoutIdInR(xml) + ", null);");
					buffer.append("\n\t\t\t" + titleBarModel.getComponentName()
							+ " = inflater.inflate(R.layout."
							+ titleBarModel.getRidName() + ", null);\n");
					buffer.append("\n\t\t\tLinearLayout mNKRootLayout = new LinearLayout(this);");
					buffer.append("\n\t\t\tmNKRootLayout.setOrientation(LinearLayout.VERTICAL);");
					buffer.append("\n\t\t\tmNKRootLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));");
					buffer.append("\n\t\t\tmNKRootLayout.addView("
							+ titleBarModel.getComponentName()
							+ ", new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));");
					buffer.append("\n\t\t\tmNKRootLayout.addView("
							+ NKCommon.CONTENT_VIEW
							+ ", new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));");
					buffer.append("\n\t\t\tsetContentView(mNKRootLayout);\n");

				}
				// Don't have navi bar
				else {
					buffer.append("\n\t\t\tsetContentView(R.layout."
							+ getLayoutIdInR(xml) + ");\n");
				}

				for (NKModel model : models) {

					if (model.isTitleBar()) {
						continue;
					}

					if (model.isTitleBarSubView()) {
						buffer.append("\n\t\t\t" + model.getComponentName()
								+ " = " + "(" + model.getClassName() + ") "
								+ titleBarModel.getComponentName()
								+ ".findViewById(" + "R.id."
								+ model.getRidName() + ");");
						continue;
					}

					if (titleBarModel != null) {
						buffer.append("\n\t\t\t" + model.getComponentName()
								+ " = " + "(" + model.getClassName() + ") "
								+ NKCommon.CONTENT_VIEW + ".findViewById("
								+ "R.id." + model.getRidName() + ");");
					} else {
						buffer.append("\n\t\t\t" + model.getComponentName()
								+ " = " + "(" + model.getClassName() + ") "
								+ "findViewById(" + "R.id."
								+ model.getRidName() + ");");
					}
				}

				// Set navi left back button onclick listener.
				if (titleBarModel != null) {

					buffer.append("\n\t\t\tmNaviLtBack.setOnClickListener(new View.OnClickListener() {");
					buffer.append("\n\t\t\t@Override");
					buffer.append("\n\t\t\tpublic void onClick(View v) {");
					buffer.append("\n\t\t\t\t\tif (v == mNaviLtBack) {");
					buffer.append("\n\t\t\t\t\t\tfinish();");
					buffer.append("\n\t\t\t\t\t}");
					buffer.append("\n\t\t\t\t}");
					buffer.append("\n\t\t\t});");
				}

				buffer.append("\n\t\t}");
				buffer.append("\n\t}\n ");
			}
			// XML => Item
			else if (xml.startsWith(xml_item_prefix)) {

				// Translate xml to item
				buffer.append("\n\n\t" + "public class " + className
						+ " {  \n ");

				// xml to compoents
				List<NKModel> models = xml2Components(xml);

				// Generate pulbic params component.
				for (NKModel model : models) {
					buffer.append("\n\t\tpublic " + model.getClassName() + " "
							+ model.getComponentName() + ";");
				}
				buffer.append("\n\n\t\tpublic " + className
						+ " (View convertView) {");

				// Init param with findViewById().
				for (NKModel component : models) {
					buffer.append("\n\t\t\t" + component.getComponentName()
							+ " = " + "(" + component.getClassName() + ")"
							+ "convertView.findViewById(" + "R.id."
							+ component.getRidName() + ");");
				}
				buffer.append("\n\t\t}");
				buffer.append("\n\t}\n ");
			}
			// XML => Fragment
			else if (xml.startsWith(xml_fragment_prefix)) {

				// Translate xml to fragment
				buffer.append("\n\n\t" + "public class " + className
						+ " extends Fragment {  \n ");

				// xml to compoents
				List<NKModel> models = xml2Components(xml);

				// Generate pulbic params component.
				for (NKModel model : models) {
					buffer.append("\n\t\tpublic " + model.getClassName() + " "
							+ model.getComponentName() + ";");
				}

				buffer.append("\n\n\t@Override");
				buffer.append("\n\n\tpublic View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {");
				buffer.append("\n\n\t\t\tView convertView = inflater.inflate(R.layout."
						+ getLayoutIdInR(xml) + ", container, false);");

				// Init param with findViewById().
				for (NKModel component : models) {
					buffer.append("\n\t\t\t" + component.getComponentName()
							+ " = " + "(" + component.getClassName() + ")"
							+ "convertView.findViewById(" + "R.id."
							+ component.getRidName() + ");");
				}
				buffer.append("\n\n\t\t\treturn convertView;");
				buffer.append("\n\t\t}");
				buffer.append("\n\t}\n ");

			}
		}

		return buffer.toString();
	}

	/**
	 * Translate xmlName to class name.eg activity_main => CLASS_NAME(JT) +
	 * MainActivity =JTMainActivity
	 * 
	 * @param xmlName
	 * @return
	 */
	private String xmlName2ClassName(String xmlName) {

		if (xmlName == null || xmlName.isEmpty()) {
			NKLog.e(TAG, "xmlName2ClassName :: xmlName is null or empty");
			System.exit(1);
		}

		// Get layout id in R
		String ridName = getLayoutIdInR(xmlName);

		// Generate class name
		String[] names = ridName.split("_");
		StringBuffer className = new StringBuffer();
		for (int i = 0; i < names.length; i++) {
			className.append(firstLetterToUpper(names[i]));
		}
		return className.toString();
	}

	/**
	 * Translate id to name. eg: @+id/rcd_voice => mRcdVoice
	 * 
	 * @param id
	 * @return
	 */
	private String id2Name(String id) {

		// JTLog.o(TAG, "Id Name :" + str);
		if (id == null || id.length() == 0) {
			NKLog.e(TAG, "id2Name :: Id is null or id length is 0!!!");
			return null;
		}

		String newStr = null;
		/**
		 * @+id/rcd_voice => rcd_voice
		 */
		newStr = getIdInR(id);

		/**
		 * rcd_voice => mRcdVoice
		 */
		String[] names = newStr.split("_");
		StringBuffer componentName = new StringBuffer();
		componentName.append("m");
		for (int i = 0; i < names.length; i++) {
			componentName.append(firstLetterToUpper(names[i]));
		}
		return componentName.toString();
	}

	/**
	 * hello => Hello
	 * 
	 * @param word
	 * @return
	 */
	private String firstLetterToUpper(String word) {
		return word.substring(0, 1).toUpperCase() + word.substring(1);
	}

	/**
	 * Change xxx.xml to xxx
	 * 
	 * @param xmlName
	 * @return
	 */
	private String getLayoutIdInR(String xmlName) {
		return xmlName.replaceAll(".xml", "");
	}

	/**
	 * Get component id in R.java eg: @+id/rcd_voice => rcd_voice
	 * 
	 * @param id
	 * @return
	 */
	private String getIdInR(String id) {
		if (id.startsWith(NKCommon.ID_ADD_PREFIX)) {
			return id.substring(NKCommon.ID_ADD_PREFIX.length());
		}
		return "";
	}

	/**
	 * Parser xml to models
	 * 
	 * @param root
	 * @return
	 */
	private List<NKModel> xml2Models(Element root) {

		NKLog.e(TAG, "xml2Models :: Root --------->> " + root.getName()
				+ " --------->> ");

		List<NKModel> models = new LinkedList<NKModel>();
		@SuppressWarnings("unchecked")
		List<Attribute> attributes = root.attributes();
		int i = 0;
		for (Attribute attribute : attributes) {
			i++;

			// Log -->
			NKLog.e(TAG, "xml2Models :: Root attribute ID = " + i + " ==>>");
			NKLog.o(TAG,
					"xml2Models :: \t\t Attribute name :" + attribute.getName());
			NKLog.o(TAG,
					"xml2Models :: \t\t Attribute value :"
							+ attribute.getValue());
			NKLog.o(TAG, "xml2Models :: \t\t Attribute nameSpacePrefix :"
					+ attribute.getNamespacePrefix());
			NKLog.o(TAG, "xml2Models :: \t\t Attribute nameSpaceURI :"
					+ attribute.getNamespaceURI());
			NKLog.e(TAG, "xml2Models :: Root attribute ID = " + i + " <<==\n");
			// Log <--

			String namespacePrefix = attribute.getNamespacePrefix();
			String namespaceURI = attribute.getNamespaceURI();
			String name = attribute.getName();
			String value = attribute.getValue();

			if (namespacePrefix.equals(name_space_prefix)
					&& namespaceURI.equals(name_space_uri)
					&& name.equals(theme_name) && value.equals(theme_value)) {

				NKLog.e(TAG, "xml2Models :: The layout contains theme!!!\n");

				// FIXME:
				NKModel titleBarModel = new NKModel();
				titleBarModel.setClassName("View");
				titleBarModel.setComponentName(navi_bar_name);
				titleBarModel.setRidName(getLayoutIdInR(navi_layout_xml));
				titleBarModel.setTitleBar(true);
				models.add(titleBarModel);

				List<NKModel> tbModels = new ArrayList<NKModel>();
				tbModels = xml2Components(navi_layout_xml);
				for (NKModel nkModel : tbModels) {
					nkModel.setTitleBarSubView(true);
					models.add(nkModel);
				}
			}

			if (name.equals(NKCommon.ID)) {
				
				// FIXME: Some bug hide here.
				if (value.startsWith(NKCommon.ID_ADD_PREFIX)
						|| value.startsWith(NKCommon.ID_PREFIX)
						|| value.startsWith(NKCommon.ID_ANDROID_PREFIX)) {
					NKModel model = new NKModel();
					model.setComponentName(id2Name(value));
					model.setClassName(root.getName());
					model.setRidName(getIdInR(value));
					models.add(model);
				}
			}
		}

		// Log
		NKLog.e(TAG,
				"xml2Models :: ################   Root Model    ################ ");
		for (NKModel compo : models) {
			NKLog.o(TAG, "xml2Models :: ClassName:" + compo.getClassName()
					+ " ||| Name:" + compo.getComponentName() + " ||| Rid:"
					+ compo.getRidName());
		}
		NKLog.e(TAG,
				"xml2Models :: ################   Root Model    ################\n");

		NKLog.e(TAG, "xml2Models :: Root <<--------- " + root.getName()
				+ " <<---------\n");

		@SuppressWarnings("unchecked")
		List<Element> elements = root.elements();
		i = 0;
		for (Element element : elements) {
			i++;

			NKLog.e(TAG,
					"xml2Models :: Element ID = " + i + " ==>> "
							+ element.getName());

			boolean isSubLayout = false;
			for (String layout : layout_root_list) {
				if (element.getName().equals(layout)) {

					NKLog.e(TAG,
							"xml2Models :: ============>> SubLayout ============>>");

					Element subRoot = element;

					NKLog.e(TAG,
							"xml2Models :: >>>>>>>>>>>>>>>>>>> SubLayout models >>>>>>>>>>>>>>>>>>> ");

					List<NKModel> subLayoutModels = xml2Models(subRoot);

					NKLog.e(TAG,
							"xml2Models :: <<<<<<<<<<<<<<<<<<<< SubLayout models <<<<<<<<<<<<<<<<<<<< ");

					for (NKModel model : subLayoutModels) {
						NKLog.o(TAG,
								"xml2Models :: ClassName:"
										+ model.getClassName() + " ||| Name:"
										+ model.getComponentName()
										+ " ||| Rid:" + model.getRidName());
					}

					models.addAll(subLayoutModels);
					isSubLayout = true;

					NKLog.e(TAG,
							"xml2Models :: <<============ SubLayout <<============");

					break;
				}
			}

			// Elemnet is subLayout, so contine;
			if (isSubLayout) {
				continue;
			}

			// Element is include subLayout
			if (element.getName().equals("include")) {

				NKLog.e(TAG,
						"xml2Models :: **************>> Include **************>> ");

				@SuppressWarnings("unchecked")
				List<Attribute> attributes1 = element.attributes();
				int j = 0;
				for (Attribute attribute : attributes1) {
					j++;
					// Log
					NKLog.o(TAG, "xml2Models :: \t\t Include attribute ID ="
							+ j + " ==>>");
					NKLog.o(TAG, "xml2Models :: \t\t Attribute name :"
							+ attribute.getName());
					NKLog.o(TAG, "xml2Models :: \t\t Attribute value :"
							+ attribute.getValue());
					NKLog.o(TAG, "xml2Models :: \t\t Include attribute ID ="
							+ j + " <<==");

					String name = attribute.getName();
					String value = attribute.getValue();
					if (name.equals("layout")) {
						String includeXmlName = getIncludeXmlName(value);
						List<NKModel> includeComponents = xml2Components(includeXmlName);
						models.addAll(includeComponents);
					}
				}

				NKLog.e(TAG,
						"xml2Models :: <<************** Include <<**************");

			} else {
				@SuppressWarnings("unchecked")
				List<Attribute> attributes1 = element.attributes();
				int j = 0;
				for (Attribute attribute : attributes1) {
					j++;

					// Log
					NKLog.e(TAG, "xml2Models :: \t Element ID = " + i
							+ "  attribute ID = " + j + " ==>> ");
					NKLog.o(TAG, "xml2Models :: \t\t Attribute name :"
							+ attribute.getName());
					NKLog.o(TAG, "xml2Models :: \t\t Attribute value :"
							+ attribute.getValue());
					NKLog.e(TAG, "xml2Models :: \t Element ID = " + i
							+ "  attribute ID = " + j + " <<==\n");

					String name = attribute.getName();
					String value = attribute.getValue();
					if (name.equals("id")
							&& value.startsWith(NKCommon.ID_ADD_PREFIX)) {
						NKModel model = new NKModel();
						model.setComponentName(id2Name(value));
						model.setClassName(element.getName());
						model.setRidName(getIdInR(value));
						models.add(model);
					}
				}
			}

			NKLog.e(TAG, "xml2Models :: Element ID = " + i + " <<==  "
					+ element.getName() + "\n");

		}
		return models;
	}

	/**
	 * Parser xml to component model, generate models list.
	 * 
	 * @param xmlName
	 * @return
	 */
	private List<NKModel> xml2Components(String xmlName) {

		if (xmlName == null || xmlName.isEmpty()) {
			NKLog.e(TAG, "xml2Components :: xmlName is null or empty");
			System.exit(1);
		}

		NKLog.e(TAG, "xml2Components :: >>>>>>>>>>>>>>>>>>>> " + xmlName
				+ " >>>>>>>>>>>>>>>>>>>>");

		List<NKModel> models = new LinkedList<NKModel>();
		SAXReader reader = new SAXReader();
		try {
			Document doc = reader.read(new File(layouts_dir + "/" + xmlName));
			Element root = doc.getRootElement();
			models = xml2Models(root);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		NKLog.e(TAG, "xml2Components :: <<<<<<<<<<<<<<<<<<<< " + xmlName
				+ " <<<<<<<<<<<<<<<<<<<<\n");

		return models;
	}

	private String getIncludeXmlName(String value) {

		if (value == null || value.isEmpty()) {
			NKLog.e(TAG, "getIncludeXmlName :: value is null or empty");
			System.exit(1);
		}

		String xmlName = "";
		xmlName = value.substring(NKCommon.INCLUDE_LAYOUT_PREFIX.length())
				+ ".xml";
		NKLog.o(TAG, "getIncludeXmlName :: getComponents :: Include layout == "
				+ xmlName);

		return xmlName;
	}
}
