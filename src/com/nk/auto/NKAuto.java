package com.nk.auto;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileSystemView;

public class NKAuto {

	private static final String PROJECT_PROP_NAME = "project.properties";

	public static void main(String[] args) {
		
		String package_name;
		String project_dir;
		String root_name = "S";
		String author = "Tu";
		String name_space_prefix = "stream";

		// Theme
		String theme_name = "theme";
		String theme_value = "titlebar";

		// Navi
		String navi_layout_xml = "play_navi_bar.xml";
		String navi_bar_name = "mNaviBarView";
		
		final HashMap<String, String> params = readProperties();
		package_name = params.get(NKCommon.PACKAGE_NAME);
		project_dir = params.get(NKCommon.PROJECT_DIR);
		root_name = params.get(NKCommon.ROOT_NAME);
		author = params.get(NKCommon.AUTHOR);
		name_space_prefix = params.get(NKCommon.NAME_SPACE_PREFIX);

		JFrame frame = new JFrame("Auto generate Tool");
		frame.setSize(500, 300);
		frame.setResizable(true);
		frame.setLocationRelativeTo(frame.getOwner());
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowListener() {

			@Override
			public void windowOpened(WindowEvent event) {

			}

			@Override
			public void windowIconified(WindowEvent event) {

			}

			@Override
			public void windowDeiconified(WindowEvent event) {

			}

			@Override
			public void windowDeactivated(WindowEvent event) {

			}

			@Override
			public void windowClosing(WindowEvent event) {
				System.exit(1);
			}

			@Override
			public void windowClosed(WindowEvent event) {

			}

			@Override
			public void windowActivated(WindowEvent event) {

			}
		});

		JPanel rootPanel = new JPanel();
		BoxLayout rootlLayout = new BoxLayout(rootPanel, BoxLayout.Y_AXIS);
		rootPanel.setLayout(rootlLayout);
		/**
		 * Project path
		 */
		final JPanel pathPanel = new JPanel(new FlowLayout());
		// Label
		JLabel pathLabel = new JLabel("Project Path:");
		pathPanel.add(pathLabel);
		// TextField
		final JTextField pathTextfield = new JTextField();
		pathTextfield.setText(project_dir);
		pathTextfield.setColumns(20);
		pathPanel.add(pathTextfield);
		// Choose button
		JButton pathButton = new JButton("...");
		final JFileChooser fc = new JFileChooser();
		FileSystemView fsv = FileSystemView.getFileSystemView();
		fc.setCurrentDirectory(fsv.getDefaultDirectory());
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		pathButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int intRetVal = fc.showOpenDialog(pathPanel);
				if (intRetVal == JFileChooser.APPROVE_OPTION) {
					pathTextfield.setText(fc.getSelectedFile().getPath());
				}
			}
		});
		pathPanel.add(pathButton);
		rootPanel.add(pathPanel);
		/**
		 * Package name
		 */
		JPanel namePanel = new JPanel(new FlowLayout());
		// Label
		JLabel nameLabel = new JLabel("Package Name:");
		namePanel.add(nameLabel);
		JTextField nameTextField = new JTextField();
		nameTextField.setText(package_name);
		nameTextField.setColumns(20);
		namePanel.add(nameTextField);
		rootPanel.add(namePanel);

		package_name = nameTextField.getText().trim();
		project_dir = pathTextfield.getText().trim();
		/**
		 * Data which input by user.
		 */
		params.put(NKCommon.PACKAGE_NAME, package_name);
		params.put(NKCommon.PROJECT_DIR, project_dir);
		params.put(NKCommon.ROOT_NAME, root_name);
		params.put(NKCommon.AUTHOR, author);
		params.put(NKCommon.NAME_SPACE_PREFIX, name_space_prefix);

		// Theme
		params.put(NKCommon.THEME_NAME, theme_name);
		params.put(NKCommon.THEME_VALUE, theme_value);

		// Navi
		params.put(NKCommon.NAVI_LAYOUT_XML, navi_layout_xml);
		params.put(NKCommon.NAVI_BAR_NAME, navi_bar_name);

		// Imports
		final ArrayList<String> import_list = new ArrayList<String>();
		import_list.add(package_name + ".R");
		import_list.add("android.os.*");
		import_list.add("android.view.*");
		import_list.add("android.widget.*");
		import_list.add("android.webkit.*");
		import_list.add("android.content.Context");
		import_list.add("android.app.*");
		import_list.add("android.support.v4.app.*");
		import_list.add("android.support.v4.app.Fragment");
		import_list.add("android.widget.RelativeLayout.LayoutParams");
		Collections.sort(import_list);

		// XML layout name prefix
		String xml_activity_prefix = "play_activity";
		String xml_item_prefix = "play_item";
		String xml_fragment_prefix = "play_fragment";
		String xml_fragmentactivity_prefix = "play_activityf";
		final HashMap<String, String> xml_prefix_map = new HashMap<String, String>();
		xml_prefix_map.put(NKCommon.XML_ACTIVITY_PREFIX, xml_activity_prefix);
		xml_prefix_map.put(NKCommon.XML_ITEM_PREFIX, xml_item_prefix);
		xml_prefix_map.put(NKCommon.XML_FRAGMENT_PREFIX, xml_fragment_prefix);
		xml_prefix_map.put(NKCommon.XML_FRAGMENTACTIVITY_PREFIX,
				xml_fragmentactivity_prefix);

		// Layout roots
		final ArrayList<String> layout_root_list = new ArrayList<String>();
		// Android system root layout.
		layout_root_list.add("LinearLayout");
		layout_root_list.add("RelativeLayout");
		layout_root_list.add("GridLayout");
		layout_root_list.add("FrameLayout");
		layout_root_list.add("Fragment");
		layout_root_list.add("TableLayout");
		layout_root_list.add("TableRow");
		layout_root_list.add("ScrollView");
		layout_root_list.add("android.support.v4.view.ViewPager");
		layout_root_list.add("RadioGroup");
		// User root layout.
		layout_root_list.add("com.blsm.khl.view.ExtendedViewPager");
		Collections.sort(layout_root_list);

		// ActivityGroup
		final ArrayList<String> activity_group_list = new ArrayList<String>();
		activity_group_list.add("TabHost");
		/**
		 * Generate button
		 */
		JButton genButton = new JButton("Generate");
		genButton.setBackground(Color.GREEN);
		final HashMap<String, String> finalParamsMap = params;
		genButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				writeProperties(params);
				// Generate auto java
				new NKGenerate().generate(finalParamsMap, import_list, xml_prefix_map,
						layout_root_list, activity_group_list);
			}
		});
		rootPanel.add(genButton);
		/**
		 * Add root panel
		 */
		frame.add(rootPanel);
		frame.setVisible(true);
	}

	public static String readValue(String key) {

		Properties properties = new Properties();
		try {
			InputStream is = new BufferedInputStream(new FileInputStream(
					PROJECT_PROP_NAME));
			properties.load(is);
		} catch (FileNotFoundException e) {
			NKLog.t(e);
		} catch (IOException e) {
			NKLog.t(e);
		}

		String value = properties.getProperty(key);
		if (value == null || value.isEmpty()) {
			value = "";
		}
		return value;
	}

	public static HashMap<String, String> readProperties() {

		HashMap<String, String> params = new HashMap<String, String>();
		Properties properties = new Properties();
		try {
			InputStream is = new BufferedInputStream(new FileInputStream(
					PROJECT_PROP_NAME));
			properties.load(is);
			Enumeration<?> keys = properties.propertyNames();
			while (keys.hasMoreElements()) {
				String key = (String) keys.nextElement();
				String value = properties.getProperty(key);
				params.put(key, value);
			}
		} catch (FileNotFoundException e) {
			NKLog.t(e);
		} catch (IOException e) {
			NKLog.t(e);
		}
		return params;
	}

	public static void writeValue(String key, String value) {

		Properties properties = new Properties();
		try {
			InputStream is = new FileInputStream(PROJECT_PROP_NAME);
			properties.load(is);
			OutputStream fos = new FileOutputStream(PROJECT_PROP_NAME);
			properties.setProperty(key, value);
			properties.store(fos, "Update '" + key + "' value");
		} catch (Exception e) {
			NKLog.t(e);
		}

	}

	public static void writeProperties(HashMap<String, String> params) {

		Properties properties = new Properties();
		try {
			InputStream is = new FileInputStream(PROJECT_PROP_NAME);
			properties.load(is);
			OutputStream os = new FileOutputStream(PROJECT_PROP_NAME);
			Set<String> keys = params.keySet();
			String value = "";
			for (String key : keys) {
				value = params.get(key);
				properties.setProperty(key, value);
				properties.store(os, "");
			}
		} catch (Exception e) {
			NKLog.t(e);
		}

	}
}
