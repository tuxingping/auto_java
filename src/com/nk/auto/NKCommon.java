package com.nk.auto;

public interface NKCommon {

	public static boolean DEBUG = true;
	
	// Names
	public static final String ANDROID_NAMESPACE = "http://schemas.android.com/apk/res/";
	public static final String PACKAGE_NAME = "package_name";
	public static final String PROJECT_DIR = "project_dir";
	public static final String ROOT_NAME = "root_name";
	public static final String AUTHOR = "author";
	public static final String NAME_SPACE_PREFIX = "name_space_prefix";

	// Theme
	public static final String THEME_NAME = "theme_name";
	public static final String THEME_VALUE = "theme_value";
	
	// Navi
	public static final String NAVI_LAYOUT_XML = "navi_layout_xml";
	public static final String NAVI_BAR_NAME = "navi_bar_name";
	
	// XML layout name prefix
	public static final String XML_ACTIVITY_PREFIX = "xml_activity_prefix";
	public static final String XML_ITEM_PREFIX = "xml_item_prefix";
	public static final String XML_FRAGMENT_PREFIX = "xml_fragment_prefix";
	public static final String XML_FRAGMENTACTIVITY_PREFIX = "xml_fragmentactivity_prefix";
	
	// ID
	public static final String ID = "id";
	public static final String ID_PREFIX = "@id/";
	public static final String ID_ADD_PREFIX = "@+id/";
	public static final String ID_ANDROID_PREFIX = "@android:id/";
	
	// Layout
	public static final String INCLUDE_LAYOUT_PREFIX = "@layout/";
	
	// Content view
	public static final String CONTENT_VIEW = "mContentView";
}
