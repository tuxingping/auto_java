package com.nk.auto;

public class NKLog {

	public static void o(String tag, String msg) {
		if (NKCommon.DEBUG) {
			try {
				System.out.println("Tag: " + tag + "  |----| " + msg);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void e(String tag, String msg) {
		if (NKCommon.DEBUG) {
			try {
				System.err.println("Tag: " + tag + "  |----| " + msg);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void t(Exception e) {
		if (NKCommon.DEBUG) {
			try {
				e.printStackTrace();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}
}
